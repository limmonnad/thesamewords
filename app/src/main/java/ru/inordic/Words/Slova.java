package ru.inordic.Words;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Slova {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите 2 строки");
        String s1 = scanner.nextLine();
        String s2 = scanner.nextLine();
        Slova slova = new Slova();
        slova.sameWords(s1, s2);
    }

    public String removePunctuationMarks(String s) {
        char[] c = s.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (!Character.isLetter(c[i]) && !Character.isWhitespace(c[i]))
                c[i] = ' ';
        }
        String onlyAbc = new String(c);
        return onlyAbc;
    }


    public  Set<String> fromStringToSetWords(String s) {
        String after = s.trim().replaceAll(" +", " ");//оставить только пробелы
        String[] array = after.split(" ");// в массив
        Set<String> strings = new HashSet<>(Arrays.asList(array));// в сет
        return strings; // вернуть сет
    }

    public Set<String> sameWords (String a, String b){
        String s11 = removePunctuationMarks(a);
        String s22 = removePunctuationMarks(b);
        Set<String> a1 = fromStringToSetWords(s11);
        Set<String> a2 = fromStringToSetWords(s22);
        Set<String> sovpad = new HashSet<>(a1);
        sovpad.retainAll(a2);//retain all оставить совпадающие значения из 2х сетов
        System.out.println("совпадающие слова в строках");
        for (String s : sovpad) {
            System.out.print(s + " ");
        }
        return sovpad;
    }
}

