package ru.inordic.Words;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class SlovaTest {


    @Test
    void sameWordsTest() {
        Slova slova = new Slova();
        String s1 = "привет!!!";
        String s2 = "привет!!! fff ttt";
        HashSet<String> strings = new HashSet<>();
        strings.add("привет");
        Assertions.assertEquals(strings, slova.sameWords(s1, s2));
    }


}